# SiriusStorage

[![Build Status](https://travis-ci.org/labsirius/SiriusStorage.svg?branch=master)](https://travis-ci.org/labsirius/SiriusStorage)
[![dependencies Status](https://david-dm.org/labsirius/SiriusStorage/status.svg)](https://david-dm.org/labsirius/SiriusStorage)
[![Github Issues](https://img.shields.io/github/issues/labsirius/SiriusStorage.svg)](http://github.com/labsirius/SiriusStorage/issues)

## Requeriments/dependencies
* [NodeJS](https://nodejs.org/en/)

## Installation
```bash
npm install or yarn install
```

## Run
```bash
npm start or yarn start
```

## RESTful API
| url                     | method   | description                | params        |
| ----------------------- | -------- | -------------------------- | ------------- |
| /storage/img            | POST     | save a new img             | <ul><li>img</li><li>filename</li></ul> |
| /storage/img/           | PUT      | modifies a img             | <ul><li>img</li><li>filename</li></ul> |
| /storage/img/:name/     | DELETE   | delete a img               |                       |
| /storage/docs/           | POST     | save a new doc             | <ul><li>doc</li><li>filename</li></ul> |
| /storage/docs/           | PUT      | modifies a doc             | <ul><li>doc</li><li>filename</li></ul> |
| /storage/docs/:name/     | DELETE   | delete a doc               |                       |
