const path = require("path");
const multer = require("multer");
const helpers = require("../helpers");
const upload = multer({ dest: "files/img" }).single("photo");

exports.saveImg = (req, res) => {
  upload(req, res, err => {
    if (err) console.log("Error: ", err);
    if (req.file) {
      const dir = __dirname + "/../files/img";
      const { filename } = req.file;
      const ext = path.extname(req.file.originalname);
      const newName = req.body.filename + ext;

      helpers
        .renameFile(dir, filename, newName)
        .then(() => {
          res.status(201).json({
            success: true,
            data: `${HOST}/storage/files/img/${newName}`
          });
        })
        .catch(err => {
          res
            .status(500)
            .json({ success: false, message: "Problem saving the img" });
        });
    } else {
      res.status(403).json({ success: false, message: "File empty" });
    }
  });
};

exports.deleteImg = (req, res) => {
  const dir = __dirname + "/../files/img";
  const filename = req.params.name;

  helpers
    .deleteFile(dir, filename)
    .then(() => {
      res.status(204).json({ success: true });
    })
    .catch(err => {
      res
        .status(500)
        .json({ success: false, message: "Problem deleting the img" });
    });
};
