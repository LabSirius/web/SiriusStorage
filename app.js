// Modules
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const logger = require("morgan");
const path = require("path");
const routes = require("./routes");
require("colors");

const env = process.env.NODE_ENV || "dev";
const config = require("./config").environment[env];
global.HOST = config.url;

const app = express();

app.use(
  cors({
    origin: true,
    credentials: true
  })
);

app.use(logger("dev"));
app.use(bodyParser.json());
app.use("/storage/files", express.static(path.join(__dirname, "files")));
routes(app, "/storage");

// / catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  res.status(404).json({ ok: "false", error: "not found" });
});

module.exports = app;
