module.exports = {
  environment: {
    prod: {
      url: "http://sirius.utp.edu.co"
    },
    dev: {
      url: "http://localhost:4500"
    }
  }
};
