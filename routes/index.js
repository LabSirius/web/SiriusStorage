const express = require("express");
const router = express.Router();
const { img: imgController, docs: docsController } = require("../controllers");

module.exports = (app, mountPoint) => {
  router.get("/", (req, res) => {
    res.json({ success: true, messages: "Welcome to Sirius Storage" });
  });

  // Img routes
  router.post("/img", imgController.saveImg);
  router.put("/img", imgController.saveImg);
  router.delete("/img/:name/", imgController.deleteImg);

  // Docs routes
  router.post("/docs", docsController.saveDoc);
  router.put("/docs", docsController.saveDoc);
  router.delete("/docs/:name/", docsController.deleteDoc);

  app.use(mountPoint, router);
};
