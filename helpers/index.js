const fs = require("fs");

exports.renameFile = (path, oldName, newName) => {
  return new Promise((resolve, reject) => {
    fs.rename(`${path}/${oldName}`, `${path}/${newName}`, err => {
      if (err) return reject(err);
      resolve();
    });
  });
};

exports.deleteFile = (path, filename) => {
  return new Promise((resolve, reject) => {
    fs.unlink(`${path}/${filename}`, err => {
      if (err) return reject(err);
      resolve();
    });
  });
};

exports.ensureExists = (path, filename) => {
  return new Promise((resolve, reject) => {
    fs.stat(`${path}/${filename}`, (err, stat) => {
      if (err === null) {
        return resolve(true);
      } else if (err.code === "ENOENT") {
        return resolve(false);
      }
      reject(err);
    });
  });
};
